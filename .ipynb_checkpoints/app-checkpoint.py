import app_ml
from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/')
def root():
    return render_template('index.html')


@app.route('/submit', methods=['POST'])
def submit():
    glu = request.form.get('glu')
    bp = int(request.form.get('bp'))
    sk = int(request.form.get('sk'))
    In = int(request.form.get('In'))
    BMI = int(request.form.get('BMI'))
    DI = int(request.form.get('DI'))
    age = int(request.form.get('age'))
 

    print(request.form)

    charges = app_ml.predict(glu, bp, sk , In , BMI ,DI , age )

    return render_template("result.html", charges=charges, name=name)
    # return str(charges)


app.run(host='0.0.0.0', port=4000, debug=True)
