import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras import layers 
import os
import pickle
from keras.preprocessing import image
from keras.models import load_model
import matplotlib.pyplot as plt
from keras_preprocessing.image import ImageDataGenerator
from keras.layers import Dense
from keras.models import Sequential
from keras.layers import Flatten
from keras.layers import MaxPooling2D
from keras.layers import Convolution2D

def train():
    thisdict = {
        0: "Apple___Apple_scab",
        1: "Apple___Black_rot",
        2: "Apple___Cedar_apple_rust",
        3: "Apple___healthy",
        4: "Blueberry___healthy",
        5: "Cherry_(including_sour)___healthy",
        6: "Cherry_(including_sour)___Powdery_mildew",
        7: "Corn_(maize)___Cercospora_leaf_spot Gray_leaf_spot",
        8: "Corn_(maize)___Common_rust_",
        9: "Corn_(maize)___healthy",
        10: "Corn_(maize)___Northern_Leaf_Blight",
        11: "Grape___Black_rot",
        12: "Grape___Esca_(Black_Measles)",
        13: "Grape___healthy",
        14: "Grape___Leaf_blight_(Isariopsis_Leaf_Spot)",
        15: "Orange___Haunglongbing_(Citrus_greening)",
        16: "Peach___Bacterial_spot",
        17: "Peach___healthy",
        18: "Pepper,_bell___Bacterial_spot",
        19: "Pepper,_bell___healthy",
        20: "Potato___Early_blight",
        21: "Potato___healthy",
        22: "Potato___Late_blight",
        23: "Raspberry___healthy"
    }

    model = Sequential()
    model.add(Convolution2D(filters=32,kernel_size=(3,3),activation='relu',input_shape=(64, 64, 3)))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Convolution2D(filters=32,kernel_size=(3,3),activation='relu',))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Flatten())
    model.add(Dense(units=128, activation='relu'))
    model.add(Dense(units=60, activation='relu'))
    model.add(Dense(units=24, activation='softmax'))
    model.compile(loss='categorical_crossentropy',optimizer='adam', metrics=['accuracy'])  
    m = load_model('DiseaseSava.h5')
    train_datagen = ImageDataGenerator(rescale=1./255,shear_range=0.2,zoom_range=0.2,horizontal_flip=True)
    test_datagen = ImageDataGenerator(rescale=1./255)
    training_set = train_datagen.flow_from_directory('DataSet/TrainingDataset/',target_size=(64, 64),batch_size=32,class_mode='categorical')
    test_set = test_datagen.flow_from_directory('DataSet/TestDataset/',target_size=(64, 64),batch_size=32,class_mode='categorical')

    model_predict = m.predict(test_image2)


    num = np.array(model_predict[0])  
    np.set_printoptions(precision = 3, suppress = True) 
    print(num)

    print("Number of Categories:",len(num))
    print("Category Number:",n)
    print("Leaf and Disease Name:",thisdict[n])


    model.fit_generator(training_set,epochs=1,validation_data=test_set)
    file=open("model.pkl","wb")
    pickle.dump(model_predict,file)


train()







































def predict(glu, bp, sk , In , BMI ,DI , age):

    # load the model
    import pickle

    filename = ''
    if algorithm == 0:
        filename = 'model.pkl'

    with open(filename, 'rb') as file:
        model = pickle.load(file)

    charges = model.predict([[glu, bp, sk , In , BMI ,DI , age]])

    return charges[0]




